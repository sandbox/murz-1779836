
Login by URL README

CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Installation
  * Configuration
  * Usage

INTRODUCTION
------------
Maintainer: Alexey Korepov (http://drupal.org/user/157092)

Thanks for Daniel Braksator (http://drupal.org/user/134005), author of 'Login one time'
module, for the idea and parts of code.

This project is abandoned.


INSTALLATION
------------
1. Copy login_one_time folder to modules directory (usually sites/all/modules).
2. At admin/modules enable the Login one time module.
3. Enable permissions at admin/people/permissions.


CONFIGURATION
-------------
There are no configuration page at now. You can chage default expire time of login link via
variable_set('login_by_url_expiry', $seconds);


USAGE
-----
This is API only interface at now.

You can get link url via API function:
login_by_url_generate_url($account,$path=NULL);
where:
$account - is user account object or numeric uid value.
$path - is optional path to redirect after successfull login.

Via this link user will can login to site without entering username and password.

Example:
$url = login_by_url_generate_url(1,'node/1/edit');
